# Fonts of interest

## ABeeZee

clear, round, sans. didactic. roman and italic.

## Play

square, coherent. Latin, Cyrillic, Greek. Regular and Bold. Sans.

The square makes the Greek weak.

Could we add an italic or a slab?

## Questrial

blessed by Dave Crossland, Laura Meseguer, Lasse Fister, Viviana Monsalve

1 style. Latin only (but full, including African and Vietnamese).

Is there a flat descender problem when rendering the g anti-aliased?

## Nunito

Rounded-terminal sans (Nunito), paired with a more ordinary blunt-terminal
sans (Nunito Sans).

Latin and Cyrillic.

Could we add serifs? There are too many weights (is it variable?)

## Titillium Web

A learning collaboration from the Accademia di Belle Arti di Urbino.
Seems kinda cool.

## Suez One

Hebrew and Latin display/headline font with only one style.
And a radical dot angle on the i.

## Junction

from TheLeagueOf

Light/Regular/Bold Sans. Good structure. Add Serif or slab or italic?

## Hammersmith One

Good poster font that can replace Gill Sans? Only one weight.
Lower case numbers?
